/*
This file is part of esp8266_beercounter.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <Arduino.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <ClickEncoder.h>
#include <Ticker.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <FS.h>

#include "config.h"
#include "strings.h"
#include "ui.h"
#include "navigation.h"
#include "user.h"
#include "secrets.h"

ESP8266WebServer server(80);

LiquidCrystal_I2C lcd(I2C_ADDR, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);

ClickEncoder encoder(PIN_CLK, PIN_DT, PIN_SW, ENCODER_STEPS);
Ticker encoderTicker;
Ticker dimTicker;

int16_t val, last;	// encoder support
uint8_t curBacklight;
uint64_t timeZero;  // screen dimming

ScreenType inScreen;

uint8_t numUsers;
uint8_t curUser;
SizeMarkerPos curSizeSel = SizeMarkerPos::sm;
UserData userData[MAX_USERS];

void encoderCb();
void dimCb();

void handleUserAdd();
void handleUserDelete();
void handleUserList();

void setup() {
	delay(1000);
	Serial.begin(9600);
	SPIFFS.begin();
	WiFi.mode(WIFI_AP);
	WiFi.softAP(WIFI_SSID);
	encoderTicker.attach_ms(1, encoderCb);  // every millisecond
	dimTicker.attach(1, dimCb);  // every second
	last = 0;
	lcd.begin(LCD_COLS, LCD_ROWS);
	lcd.createChar((uint8_t)0, (uint8_t *)rightArrow_c);
	lcd.createChar((uint8_t)1, (uint8_t *)leftArrow_c);
	welcomeScreen(lcd);
	numUsers = initUserData(SPIFFS, userData);
	curUser = 0;
	inScreen = ScreenType::home;
	timeZero = millis();
	curBacklight = LCD_BACKLIGHT_FULL;
	server.serveStatic(WEBSITE_ROOT, SPIFFS, WEBSITE_ROOT_TEMPLATE, WEBSITE_CACHE_HEADER);
	server.on(WEBSITE_USERADD, handleUserAdd);
	server.on(WEBSITE_USERDEL, handleUserDelete);
	server.on(WEBSITE_USERLIST, handleUserList);
	server.begin();
}

void loop() {
	bool resetBacklight = false;
	int16_t newVal = encoder.getValue();
	val += newVal;
	if (val != last) {
		last = val;
		if (curBacklight == LCD_BACKLIGHT_FULL) {
			switch (inScreen) {
				case ScreenType::users: {
					// navigate through user data list
					uint8_t newUserId = newId(curUser, numUsers - 1 , newVal);
					if (newUserId != curUser) {
						menuLine(lcd, userData[newUserId].userName, !userData[newUserId].isFirst, !userData[newUserId].isLast);
						curUser = newUserId;
					}
					break;
				};
				case ScreenType::entry: {
					SizeMarkerPos newSizeSel = newMarkerPos(curSizeSel, newVal);
					if (newSizeSel != curSizeSel) {
						entryLine(lcd, newSizeSel);
						curSizeSel = newSizeSel;
					}
					break;
				};
				default: break;
			}
		}
		resetBacklight = true;
	}
	ClickEncoder::Button b = encoder.getButton();
	if (b != ClickEncoder::Open) {
		if (b == ClickEncoder::Clicked) {
			if (curBacklight != LCD_BACKLIGHT_NONE) {
				switch (inScreen) {
					// handle button click on different screens
					case ScreenType::home: {
						// on home screen click launches user list
						userListScreen(lcd, userData[0]);
						inScreen = ScreenType::users;
						break;
					};
					case ScreenType::users: {
						// on user list click launches selected user stats
						userStatsScreen(lcd, userData[curUser]);
						inScreen = ScreenType::stats;
						break;
					};
					case ScreenType::stats: {
						// on stats screen click launches data entry screen
						userEntryScreen(lcd, userData[curUser]);
						inScreen = ScreenType::entry;
						break;
					};
					case ScreenType::entry: {
						// on entry screen click launches user list with curren user selected
						bool dataChanged = true;
						switch (curSizeSel) {
							case SizeMarkerPos::sm: {
								++userData[curUser].smallBeers;
								break;
							};
							case SizeMarkerPos::md: {
								++userData[curUser].mediumBeers;
								break;
							};
							case SizeMarkerPos::lg: {
								++userData[curUser].largeBeers;
								break;
							};
							case SizeMarkerPos::esc: {
								dataChanged = false;
								break;
							}
						};
						if (dataChanged) {
							saveUserData(SPIFFS, userData, numUsers);
						}
						userListScreen(lcd, userData[curUser]);
						inScreen = ScreenType::users;
						curSizeSel = SizeMarkerPos::sm;
						break;
					}
				}
			}
			resetBacklight = true;
		}
	}
	if (resetBacklight) {
		curBacklight = LCD_BACKLIGHT_FULL;
		lcd.setBacklight(curBacklight);
		timeZero = millis();
	}
	server.handleClient();
}

void encoderCb() {
	encoder.service();
}

void dimCb() {
	uint64_t curTime = millis();
	uint64_t elapsed = curTime - timeZero;
	uint8_t tgtBacklight = LCD_BACKLIGHT_FULL;
	if (elapsed >= LCD_TIME_NONE) {
		tgtBacklight = LCD_BACKLIGHT_NONE;
	}
	if (tgtBacklight != curBacklight) {
		curBacklight = tgtBacklight;
		lcd.setBacklight(tgtBacklight);
	}
}

void handleUserAdd() {
	if (server.method() == HTTP_POST) {
		uint8_t newNumUsers = addUser(userData, server.arg(USERNAME_FIELD).c_str(), numUsers);
		if (newNumUsers != numUsers) {
			numUsers = newNumUsers;
			saveUserData(SPIFFS, userData, numUsers);
		}
	}
	server.sendHeader(HEADER_LOCATION, WEBSITE_ROOT);
	server.send(RESPONSE_CODE_FOUND);
	userListScreen(lcd, userData[0]);
	curUser = 0;
}

void handleUserDelete() {
	if (server.args() > 0) {
		uint8_t userId = atoi(server.arg(USERID_FIELD).c_str());
		uint8_t newNumUsers = deleteUser(userData, userId, numUsers);
		if (newNumUsers != numUsers) {
			numUsers = newNumUsers;
			saveUserData(SPIFFS, userData, numUsers);
		}
	}
	server.sendHeader(HEADER_LOCATION, WEBSITE_ROOT);
	server.send(RESPONSE_CODE_FOUND);
	userListScreen(lcd, userData[0]);
	curUser = 0;
}

void handleUserList() {
	char resp[MAX_PAGE_SIZE];
	getUserNamesListJson(resp, MAX_PAGE_SIZE, userData, numUsers);
	server.send(RESPONSE_CODE_OK, MIMETYPE_JSON, resp);
}
