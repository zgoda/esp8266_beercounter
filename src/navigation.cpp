/*
This file is part of esp8266_beercounter.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "navigation.h"

uint8_t nextId(const uint8_t curId, const uint8_t maxId) {
	if (curId >= maxId) {
		return maxId;
	}
	return curId + 1;
}

uint8_t prevId(const uint8_t curId, const uint8_t maxId) {
	if (curId == 0) {
		return 0;
	}
	if (curId >= maxId) {
		return maxId - 1;
	}
	return curId - 1;
}

uint8_t newId(const uint8_t curId, const uint8_t maxId, const int16_t direction) {
	if (direction < 0) {
		return prevId(curId, maxId);
	}
	if (direction > 0) {
		return nextId(curId, maxId);
	}
	return curId;
}

SizeMarkerPos nextMarkerPos(const SizeMarkerPos curPos) {
	uint8_t posNum = curPos;
	uint8_t maxPos = SizeMarkerPos::esc;
	if (posNum == maxPos) {
		return SizeMarkerPos::sm;
	}
	return static_cast<SizeMarkerPos>(posNum + 1);
}

SizeMarkerPos prevMarkerPos(const SizeMarkerPos curPos) {
	uint8_t posNum = curPos;
	if (posNum == 0) {
		return SizeMarkerPos::esc;
	}
	return static_cast<SizeMarkerPos>(posNum - 1);
}

SizeMarkerPos newMarkerPos(const SizeMarkerPos curPos, const int16_t direction) {
	if (direction < 0) {
		return prevMarkerPos(curPos);
	}
	if (direction > 0) {
		return nextMarkerPos(curPos);
	}
	return curPos;
}
