/*
This file is part of esp8266_beercounter.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <stdint.h>
#include <string.h>
#include <LiquidCrystal_I2C.h>

#include "ui.h"
#include "strings.h"
#include "config.h"
#include "user.h"

void menuLine(LiquidCrystal_I2C& lcd, const char * text, const bool hasLeft, const bool hasRight, const bool clear) {
	if (clear) {
		clearLine(lcd, LCD_DATA_LINE);
	}
	char buf[USER_NAME_BUFSIZE];
	strncpy(buf, text, USER_NAME_LENGTH);
	buf[USER_NAME_LENGTH] = 0;
	lcd.setCursor(0, LCD_DATA_LINE);
	if (hasLeft) {
		lcd.write((uint8_t)1);
	} else {
		lcd.write(SPACE);
	}
	lcd.setCursor(1, LCD_DATA_LINE);
	lcd.print(buf);
	lcd.setCursor(LCD_COLS - 1, LCD_DATA_LINE);
	if (hasRight) {
		lcd.write((uint8_t)0);
	} else {
		lcd.write(SPACE);
	}
}

void titleLine(LiquidCrystal_I2C& lcd, const char * text, const bool clear) {
	if (clear) {
		clearLine(lcd, LCD_TITLE_LINE);
	}
	lcd.setCursor(0, LCD_TITLE_LINE);
	lcd.print(text);
}

void statsLine(LiquidCrystal_I2C& lcd, const uint16_t lg, const uint16_t md, const uint16_t sm, const bool clear) {
	if (clear) {
		clearLine(lcd, LCD_DATA_LINE);
	}
	uint16_t beerCount = lg + md + sm;
	float beerVol = static_cast<float>(lg * LG_BEER_CL + md * MD_BEER_CL + sm * SM_BEER_CL) / 100.0;
	char beerVol_s[10];
	dtostrf(beerVol, 9, 2, beerVol_s);
	char lineBuf[LCD_LINE_BUFSIZE];
	snprintf(lineBuf, LCD_COLS, STATS_LINE_TPL, beerCount, beerVol_s);
	lcd.setCursor(0, LCD_DATA_LINE);
	lcd.print(lineBuf);
}

void entryLine(LiquidCrystal_I2C& lcd, const uint8_t pos, const bool clear) {
	if (clear) {
		clearLine(lcd, LCD_DATA_LINE);
	}
	// display string
	char lineBuf[LCD_LINE_BUFSIZE];
	strcpy(lineBuf, ENTRY_LINE_TPL);
	lcd.setCursor(0, LCD_DATA_LINE);
	lcd.print(lineBuf);
	// display selection marker
	uint8_t markerPos = pos * 4;
	lcd.setCursor(markerPos, LCD_DATA_LINE);
	lcd.write((uint8_t)0);
}

void clearLine(LiquidCrystal_I2C& lcd, const uint8_t row) {
	for (uint8_t i = 0; i < LCD_COLS; i++) {
		lcd.setCursor(i, row);
		lcd.write(SPACE);
	}
}

void welcomeScreen(LiquidCrystal_I2C& lcd) {
	char lineBuf[LCD_LINE_BUFSIZE];
	lcd.setCursor(0, LCD_TITLE_LINE);
	strncpy(lineBuf, WELCOME_SCREEN_L1, LCD_COLS);
	lineBuf[LCD_COLS] = 0;
	lcd.print(lineBuf);
	lcd.setCursor(0, LCD_DATA_LINE);
	strncpy(lineBuf, WELCOME_SCREEN_L2, LCD_COLS);
	lineBuf[LCD_COLS] = 0;
	lcd.print(lineBuf);
}

void userListScreen(LiquidCrystal_I2C& lcd, const UserData &data) {
	lcd.clear();
	char lineBuf[LCD_LINE_BUFSIZE];
	strncpy(lineBuf, MENU_TITLE, LCD_COLS);
	lineBuf[LCD_COLS] = 0;
	titleLine(lcd, lineBuf, false);
	menuLine(lcd, data.userName, !data.isFirst, !data.isLast, false);
}

void userStatsScreen(LiquidCrystal_I2C& lcd, const UserData &data) {
	lcd.clear();
	titleLine(lcd, data.userName, false);
	statsLine(lcd, data.largeBeers, data.mediumBeers, data.smallBeers, false);
}

void userEntryScreen(LiquidCrystal_I2C& lcd, const UserData &data) {
	lcd.clear();
	titleLine(lcd, data.userName, false);
	entryLine(lcd, false);
}
