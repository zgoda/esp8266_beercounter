/*
This file is part of esp8266_beercounter.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _CONFIG_H
#define  _CONFIG_H

#include <stdint.h>

// LCD definition
static const uint8_t LCD_COLS = 16;
static const uint8_t LCD_LINE_BUFSIZE = LCD_COLS + 1;
static const uint8_t USER_NAME_LENGTH = LCD_COLS - 2;
static const uint8_t USER_NAME_BUFSIZE = USER_NAME_LENGTH + 1;
static const uint8_t LCD_ROWS = 2;
static const uint8_t LCD_TITLE_LINE = 0;
static const uint8_t LCD_DATA_LINE = 1;
static const uint8_t LCD_BACKLIGHT_FULL = 255;
static const uint8_t LCD_BACKLIGHT_NONE = 0;
static const uint32_t LCD_TIME_NONE = 90 * 1000;

// LCD I2C bus address
static const uint8_t I2C_ADDR = 0x3F;

// pin definitions (D1 is SCL, D2 is SDA)
static const uint8_t PIN_CLK = 12;  // D6
static const uint8_t PIN_DT = 13;  // D7
static const uint8_t PIN_SW = 14;  // D5

// rotary encoder stepping
static const uint8_t ENCODER_STEPS = 4;

// domain settings
static const uint8_t SM_BEER_CL = 25;
static const uint8_t MD_BEER_CL = 33;
static const uint8_t LG_BEER_CL = 50;

// web config
static const uint16_t MAX_PAGE_SIZE = 400;
static const uint16_t MAX_TEMPLATE_SIZE = 250;
static const char * const MIMETYPE_JSON = "application/json";
static const uint16_t RESPONSE_CODE_OK = 200;
static const uint16_t RESPONSE_CODE_FOUND = 302;
static const uint8_t USER_LINE_LENGTH = 9 + USER_NAME_LENGTH;
static const uint8_t USER_LINE_BUFSIZE = USER_LINE_LENGTH + 1;
static const char * const HEADER_LOCATION = "Location";
static const char * const USERNAME_FIELD = "name";
static const char * const USERID_FIELD = "uid";
static const char * const WEBSITE_CACHE_HEADER = "max-age=86400";

// webserver paths
static const char * const WEBSITE_ROOT = "/";
static const char * const WEBSITE_ROOT_TEMPLATE = "/www/index.html";
static const char * const WEBSITE_USERADD = "/add";
static const char * const WEBSITE_USERDEL = "/delete";
static const char * const WEBSITE_USERLIST = "/users";

#endif
