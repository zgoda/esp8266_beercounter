/*
This file is part of esp8266_beercounter.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _UI_H
#define  _UI_H

#include <stdint.h>
#include <LiquidCrystal_I2C.h>

#include "user.h"

void menuLine(LiquidCrystal_I2C& lcd, const char * text, const bool hasLeft, const bool hasRight, const bool clear = true);
void titleLine(LiquidCrystal_I2C& lcd, const char * text, const bool clear = true);
void statsLine(LiquidCrystal_I2C& lcd, const uint16_t lg, const uint16_t md, const uint16_t sm, const bool clear = true);
void entryLine(LiquidCrystal_I2C& lcd, const uint8_t pos = 0, const bool clear = false);
void clearLine(LiquidCrystal_I2C& lcd, const uint8_t row);

void welcomeScreen(LiquidCrystal_I2C& lcd);
void userListScreen(LiquidCrystal_I2C& lcd, const UserData &data);
void userStatsScreen(LiquidCrystal_I2C& lcd, const UserData &data);
void userEntryScreen(LiquidCrystal_I2C& lcd, const UserData &data);

#endif
