/*
This file is part of esp8266_beercounter.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <FS.h>
#include <ArduinoJson.h>
#include "user.h"

uint8_t initUserData(FS& fs, UserData data[]) {
	uint8_t last = 0;
	uint8_t numRecords = MAX_USERS;
	File f = fs.open(JSON_DATA_FILE, "r");
	if (f) {
		char userData[MAX_JSON_BYTES + 1];
		uint16 bytesRead = f.readBytes(userData, MAX_JSON_BYTES);
		if (bytesRead > 0) {
			userData[bytesRead] = 0;
			StaticJsonDocument<MAX_JSON_BYTES> jsonDoc;
			auto error = deserializeJson(jsonDoc, userData);
			if (error) {
				char userName[USER_NAME_BUFSIZE];
				for (uint8_t i = 0; i < MAX_USERS; i++) {
					data[i].userId = i;
					snprintf(userName, USER_NAME_LENGTH, USER_NAME_TEMPLATE, i + 1);
					strncpy(data[i].userName, userName, LCD_COLS);
					data[i].userName[LCD_COLS] = 0;
					last = i;
				}
			} else {
				JsonObject root = jsonDoc.to<JsonObject>();
				JsonArray people = root[JSON_KEY_ROOT];
				numRecords = people.size();
				for (uint8_t i = 0; i < numRecords; i++) {
					JsonObject person = people.getElement(i).to<JsonObject>();
					data[i].userId = i;
					strncpy(data[i].userName, person[JSON_KEY_USERNAME], LCD_COLS);
					data[i].userName[LCD_COLS] = 0;
					data[i].smallBeers = person[JSON_KEY_SMBEERS];
					data[i].mediumBeers = person[JSON_KEY_MDBEERS];
					data[i].largeBeers = person[JSON_KEY_LGBEERS];
					last = i;
				}
			}
		}
		f.close();
	}
	data[0].isFirst = true;
	data[last].isLast = true;
	return last + 1;
}

size_t saveUserData(FS& fs, UserData data[], const uint8_t numUsers) {
	StaticJsonDocument<400> jsonDoc;
	JsonObject root = jsonDoc.to<JsonObject>();
	JsonArray people = root.createNestedArray(JSON_KEY_ROOT);
	for (uint8_t i = 0; i < numUsers; i++) {
		JsonObject person = people.createNestedObject();
		person[JSON_KEY_USERNAME] = data[i].userName;
		person[JSON_KEY_SMBEERS] = data[i].smallBeers;
		person[JSON_KEY_MDBEERS] = data[i].mediumBeers;
		person[JSON_KEY_LGBEERS] = data[i].largeBeers;
	}
	size_t dataLen = measureJson(jsonDoc);
	File f = fs.open(JSON_DATA_FILE, "w");
	if (f) {
		serializeJson(jsonDoc, f);
		f.close();
	}
	return dataLen;
}

uint8_t addUser(UserData data[], const char * userName, const uint8_t numUsers) {
	uint8_t newNumUsers = numUsers;
	if (newNumUsers <= MAX_USERS - 1) {
		newNumUsers = numUsers + 1;
		data[numUsers].userId = numUsers;
		strncpy(data[numUsers].userName, userName, USER_NAME_LENGTH);
		data[numUsers].userName[USER_NAME_LENGTH] = 0;
		data[numUsers].smallBeers = 0;
		data[numUsers].mediumBeers = 0;
		data[numUsers].largeBeers = 0;
	}
	if (newNumUsers != numUsers) {
		data[numUsers].isFirst = (numUsers == 0);
		data[numUsers].isLast = true;
		if (numUsers > 0) {
			data[numUsers - 1].isLast = false;
		}
	}
	return newNumUsers;
}

uint8_t deleteUser(UserData data[], const uint8_t userId, const uint8_t numUsers) {
	uint8_t newNumUsers = numUsers;
	int8_t userIndex = -1;
	for (uint8_t i = 0; i < numUsers; i++) {
		if (data[i].userId == userId) {
			userIndex = i;
			break;
		}
	}
	if (userIndex > 0) {
		for (uint8_t i = userIndex; i < numUsers; ++i) {
			data[i] = data[i + 1];
		}
		newNumUsers = newNumUsers - 1;
	}
	return newNumUsers;
}

size_t getUserNamesListJson(char result[], const uint16_t maxLen, const UserData data[], const uint8_t numUsers) {
	size_t dataSize = 0;
	StaticJsonDocument<200> jsonDoc;
	JsonObject root = jsonDoc.to<JsonObject>();
	JsonArray people = root.createNestedArray(JSON_KEY_ROOT);
	for (uint8_t i = 0; i < numUsers; i++) {
		JsonObject person = people.createNestedObject();
		person[JSON_KEY_USERID] = data[i].userId;
		person[JSON_KEY_USERNAME] = data[i].userName;
	}
	dataSize = measureJson(jsonDoc);
	if (dataSize < maxLen) {
		serializeJson(jsonDoc, result, dataSize+1);
		return dataSize;
	}
	return 0;
}
