/*
This file is part of esp8266_beercounter.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _STRINGS_H
#define  _STRINGS_H

#include <stdint.h>

static const uint8_t rightArrow_c[8] = {
	0b00000,
	0b01000,
	0b01100,
	0b01110,
	0b01110,
	0b01100,
	0b01000,
	0b00000
};

static const uint8_t leftArrow_c[8] = {
	0b00000,
	0b00010,
	0b00110,
	0b01110,
	0b01110,
	0b00110,
	0b00010,
	0b00000
};

static const char * const WELCOME_SCREEN_L1 = "BEER COUNTER";
static const char * const WELCOME_SCREEN_L2 = "(c) 2016 zgoda";

static const char * const MENU_TITLE = "LUDZIE";

static const char SPACE = 32;

static const char * const STATS_LINE_TPL = "P: %d  L: %s";
static const char * const ENTRY_LINE_TPL = " SM  MD  LG  ESC";

#endif
