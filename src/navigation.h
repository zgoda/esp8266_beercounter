/*
This file is part of esp8266_beercounter.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _NAVIGATION_H
#define _NAVIGATION_H

#include <stdint.h>

typedef enum {
	home = 0,
	users = 1,
	stats = 2,
	entry = 3
} ScreenType;

typedef enum {
	sm = 0,
	md = 1,
	lg = 2,
	esc = 3,
} SizeMarkerPos;  // take *4 to obtain actual marker position on screen

// user list navigation
uint8_t nextId(const uint8_t curId, const uint8_t maxId);
uint8_t prevId(const uint8_t curId, const uint8_t maxId);
uint8_t newId(const uint8_t curId, const uint8_t maxId, const int16_t direction);

// size marker position navigation
SizeMarkerPos nextMarkerPos(const SizeMarkerPos curPos);
SizeMarkerPos prevMarkerPos(const SizeMarkerPos curPos);
SizeMarkerPos newMarkerPos(const SizeMarkerPos curPos, const int16_t direction);

#endif
