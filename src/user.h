/*
This file is part of esp8266_beercounter.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _DATA_H
#define  _DATA_H

#include <FS.h>
#include <stdint.h>

#include "config.h"

struct UserData {
	uint8_t userId;
	char userName[LCD_LINE_BUFSIZE];
	uint16_t smallBeers;
	uint16_t mediumBeers;
	uint16_t largeBeers;
	bool isFirst;
	bool isLast;
	UserData() {
		smallBeers = 0;
		mediumBeers = 0;
		largeBeers = 0;
		isFirst = false;
		isLast = false;
	}
};

static const uint8_t MAX_USERS = 8;

static const uint16_t MAX_JSON_BYTES = 400;
static const char * const JSON_DATA_FILE = "/people.json";
static const char * const JSON_KEY_USERID = "userId";
static const char * const JSON_KEY_USERNAME = "userName";
static const char * const JSON_KEY_SMBEERS = "smallBeers";
static const char * const JSON_KEY_MDBEERS = "mediumBeers";
static const char * const JSON_KEY_LGBEERS = "largeBeers";
static const char * const JSON_KEY_ROOT = "people";

static const char * const USER_NAME_TEMPLATE = "User %d";

uint8_t initUserData(FS& fs, UserData data[]);
size_t saveUserData(FS& fs, UserData data[], const uint8_t numUsers);
uint8_t addUser(UserData data[], const char * userName, const uint8_t numUsers);
uint8_t deleteUser(UserData data[], const uint8_t userId, const uint8_t numUsers);
size_t getUserNamesListJson(char result[], const uint16_t maxLen, const UserData data[], const uint8_t numUsers);

#endif
