Beer counter
============

Count beers you drank. A toy and an educational project (yes, I'm still learning).

Hardware
--------

* ESP8266 development board (WeMos D1 mini or NodeMCU)
* 1602 LCD
* rotary encoder with push button (5 pins)

Code
----

* use of HD44780 compatible LCD display with I2C interface
* dynamic menu structure
* use of rotary encoder for UI navigation
* store JSON structured data in SPIFFS
* use of ESP8266 soft AP
* use of ESP8266 based web server to serve static HTML page and process web forms
* dynamically load JSON structured data and display using JavaScript (no jQuery!)

License
-------

GPL v2.

Coyright: (c) 2016, Jarek Zgoda
