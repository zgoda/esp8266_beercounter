#include <cxxtest/TestSuite.h>
#include <stdint.h>

class IdNavigationTestSuite : public CxxTest::TestSuite
{
	uint8_t maxId;

public:
	void setUp() {
		maxId = 7;
	}

	void testNextIdRegular(void) {
		uint8_t curId = 1;
		uint8_t newId = nextId(curId, maxId);
		TS_ASSERT(newId == 2);
	}

	void testNextIdLast(void) {
		uint8_t curId = 7;
		uint8_t newId = nextId(curId, maxId);
		TS_ASSERT(newId == maxId);
	}

	void testNextIdPastBoundary(void) {
		uint8_t curId = 9;
		uint8_t newId = nextId(curId, maxId);
		TS_ASSERT(newId == maxId);
	}

	void testPrevIdRegular(void) {
		uint8_t curId = 2;
		uint8_t newId = prevId(curId, maxId);
		TS_ASSERT(newId == 1);
	}

	void testPrevIdFirst(void) {
		uint8_t curId = 0;
		uint8_t newId = prevId(curId, maxId);
		TS_ASSERT(newId == curId);
	}

	void testPrevIdPastBoundary(void) {
		uint8_t curId = 9;
		uint8_t newId = prevId(curId, maxId);
		TS_ASSERT(newId == maxId - 1);
	}
};
